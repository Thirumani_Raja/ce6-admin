import {
  Component,
  OnInit,
  ViewChild,
  ViewContainerRef,
  ComponentFactoryResolver
} from '@angular/core';
import { LibConfigService } from '../../modules/lib-config/services/lib-config.service';
import { ManageViewComponent } from '../../modules/manage/components/manage-view/manage-view.component';
import { SuperAccessModuleComponent } from '../../modules/manage/components/super-access-module/super-access-module.component';
import { SuperAdminBoardComponent } from '../../modules/manage/components/super-admin-board/super-admin-board.component';
import { SuperAdminSchoolComponent } from '../../modules/manage/components/super-admin-school/super-admin-school.component';
import { SuperAdminUserComponent } from '../../modules/manage/components/super-admin-user/super-admin-user.component';
import { MessageViewComponent } from '../../modules/message/components/message-view/message-view.component';
import {
  Navigation,
  SubNavigation
} from '../../modules/models/interfaces/dashboard-config.interface';
import { ConfigurationComponent } from '../../modules/over-view/components/configuration/configuration.component';
import { MyAccessComponent } from '../../modules/over-view/components/my-access/my-access.component';
import { OverviewViewComponent } from '../../modules/over-view/components/overview-view/overview-view.component';
import { QueueInfoComponent } from '../../modules/over-view/components/queue-info/queue-info.component';
import { SyncDataComponent } from '../../modules/over-view/components/sync-data/sync-data.component';
import { BuildDetailsComponent } from '../../modules/product/components/build-details/build-details.component';
import { SupportViewComponent } from '../../modules/support/components/support-view/support-view.component';
import { AdminService } from '../../services/admin.service';

@Component({
  selector: 'standalone-ce6-admin-admin-view',
  templateUrl: './admin-view.component.html',
  styleUrls: ['./admin-view.component.scss']
})
export class AdminViewComponent implements OnInit {
  @ViewChild('adminContentLib', { static: true, read: ViewContainerRef })
  adminContentLib: ViewContainerRef | undefined;
  loadConfigModules: Navigation;
  selectedTab = 'access';
  subNavigation: SubNavigation;

  userRoles;
  selectedItem;

  constructor(
    private libConfigService: LibConfigService,
    private adminService: AdminService,
    private componentFactoryResolver: ComponentFactoryResolver
  ) {
    this.adminService.getUserRoles().subscribe(data => {
      console.log('userRoles', data.userRoles);
      this.userRoles = data.userRoles;
      this.loadConfigModules = data.userRoles[0].navigation;
      this.subNavigation = data.userRoles[0].navigation[0].subnavigation;
      console.log('subNavigation', this.subNavigation);
    });

    adminService.userRoles$.subscribe(roles => {
      this.selectedItem = roles;
      console.log('selected roles', this.selectedItem);
    });

    adminService.roleNavigation$.subscribe(nav => {
      this.loadConfigModules = nav;
      console.log('navigation', nav);
    });

    adminService.roleSubNavigation$.subscribe(subnav => {
      this.subNavigation = subnav;
      console.log('sub-navigation', subnav);
    });
  }

  ngOnInit(): void {}

  loadOverView() {
    if (this.adminContentLib) {
      this.adminContentLib.clear();
    }
    this.libConfigService
      .getComponentFactory<OverviewViewComponent>('overview')
      .subscribe({
        next: componentFactory => {
          if (!this.adminContentLib) {
            return;
          }
          const ref = this.adminContentLib.createComponent(componentFactory);
        },
        error: err => {
          console.warn(err);
        }
      });
  }

  loadManage() {
    if (this.adminContentLib) {
      this.adminContentLib.clear();
    }
    this.libConfigService
      .getComponentFactory<ManageViewComponent>('manage')
      .subscribe({
        next: componentFactory => {
          if (!this.adminContentLib) {
            return;
          }
          const ref = this.adminContentLib.createComponent(componentFactory);
        },
        error: err => {
          console.warn(err);
        }
      });
  }

  loadMessage() {
    if (this.adminContentLib) {
      this.adminContentLib.clear();
    }
    this.libConfigService
      .getComponentFactory<MessageViewComponent>('message')
      .subscribe({
        next: componentFactory => {
          if (!this.adminContentLib) {
            return;
          }
          const ref = this.adminContentLib.createComponent(componentFactory);
        },
        error: err => {
          console.warn(err);
        }
      });
  }

  loadProduct() {
    if (this.adminContentLib) {
      this.adminContentLib.clear();
    }
    this.libConfigService
      .getComponentFactory<BuildDetailsComponent>('product')
      .subscribe({
        next: componentFactory => {
          if (!this.adminContentLib) {
            return;
          }
          const ref = this.adminContentLib.createComponent(componentFactory);
        },
        error: err => {
          console.warn(err);
        }
      });
  }

  loadSupport() {
    if (this.adminContentLib) {
      this.adminContentLib.clear();
    }
    this.libConfigService
      .getComponentFactory<SupportViewComponent>('support')
      .subscribe({
        next: componentFactory => {
          if (!this.adminContentLib) {
            return;
          }
          const ref = this.adminContentLib.createComponent(componentFactory);
        },
        error: err => {
          console.warn(err);
        }
      });
  }
  loadConfig() {
    if (this.adminContentLib) {
      this.adminContentLib.clear();
    }
    const componentFactory = this.componentFactoryResolver.resolveComponentFactory(
      ConfigurationComponent
    );
    this.adminContentLib.createComponent(componentFactory);
  }
  loadModulesView(value) {
    console.log(value);
    this.subNavigation = value.subnavigation;
  // this.adminService.loadNavigation(value)
    //this.adminService.loadSubNavigation(this.subNavigation)
    console.log(value.subnavigation);
    if (!value.subnavigation) {
      console.log('clear lib');
      this.adminContentLib.clear();
    }

    if (value.id === 'overview') {
      if (value.subnavigation.length === 0) {
        console.log(
          'loadConfigModules clear lib',
          this.loadConfigModules.subnavigation
        );
        this.adminContentLib.clear();
      } else {
        this.loadOverView();
      }
    } else if (value.id === 'manage') {
      if (value.subnavigation.length === 0) {
        console.log(
          'loadConfigModules clear lib',
          this.loadConfigModules.subnavigation
        );
        this.adminContentLib.clear();
      } else {
        this.loadManage();
      }
    } else if (value.id === 'sc-manage') {
      if (value.subnavigation.length === 0) {
        console.log(
          'loadConfigModules clear lib',
          this.loadConfigModules.subnavigation
        );
        this.adminContentLib.clear();
      } else {
        //this.loadScSchool();
      }
    } else if (value.id === 'messages') {
      if (value.subnavigation.length === 0) {
        console.log(
          'loadConfigModules clear lib',
          this.loadConfigModules.subnavigation
        );
        this.adminContentLib.clear();
      } else {
        this.loadMessage();
      }
    } else if (value.id === 'product') {
      if (value.subnavigation.length === 0) {
        console.log(
          'loadConfigModules clear lib',
          this.loadConfigModules.subnavigation
        );
        this.adminContentLib.clear();
      } else {
        this.loadProduct();
      }
    } else if (value.id === 'support') {
      this.loadSupport();
    } else if (value.id === 'academic-year') {
      if (value.subnavigation.length === 0) {
        console.log(
          'loadConfigModules clear lib',
          this.loadConfigModules.subnavigation
        );
        this.adminContentLib.clear();
      } else {
        // this.loadAcademicYear();
      }
    }
  }

  onUserSelection(role) {
    let userRole = role.id;
    this.adminService.loadAdminConfig(role);
    this.loadConfigModules = role.navigation;
    console.log(
      'loadConfigModules-------------------- lib',
      this.loadConfigModules[0].subnavigation
    );

    console.log(this.loadConfigModules);
    // this.checkNav(role.navigation);
    if (userRole === 'site-admin') {
      if (this.loadConfigModules[0].subnavigation.length === 0) {
        console.log(
          'loadConfigModules clear lib',
          this.loadConfigModules.subnavigation
        );
        this.adminContentLib.clear();
      }
      // this.loadManage();
    } else if (userRole === 'principal') {
      if (this.loadConfigModules[0].subnavigation.length === 0) {
        console.log(
          'loadConfigModules clear lib',
          this.loadConfigModules.subnavigation
        );
        this.adminContentLib.clear();
      } else {
        this.loadOverView();
      }
    } else if (userRole === 'super-admin') {
      if (this.loadConfigModules[0].subnavigation.length === 0) {
        console.log(
          'loadConfigModules clear lib',
          this.loadConfigModules.subnavigation
        );
        this.adminContentLib.clear();
      } else {
        this.loadOverView();
      }
    } else if (userRole === 'school-admin') {
      if (this.loadConfigModules[0].subnavigation.length === 0) {
        console.log(
          'loadConfigModules clear lib',
          this.loadConfigModules.subnavigation
        );
        this.adminContentLib.clear();
      } else {
        this.loadOverView();
      }
    } else if (userRole === 'teacher') {
      if (this.loadConfigModules[0].subnavigation.length === 0) {
        console.log(
          'loadConfigModules clear lib',
          this.loadConfigModules.subnavigation
        );
        this.adminContentLib.clear();
      } else {
        this.loadSupport();
      }
    }
  }

  switchTab(value) {
    this.selectedTab = value;
    console.log('outputTab', this.selectedTab);
    if (this.selectedTab === 'configuration') {
      console.log(this.selectedTab);
      this.loadConfig();
    } else if (this.selectedTab === 'my-access') {
      this.loadMyAccess();
      console.log(this.selectedTab);
    } else if (this.selectedTab === 'queue-info') {
      this.loadQueueInfo();
      console.log(this.selectedTab);
    } else if (this.selectedTab === 'sync-data') {
      this.loadSyncData();
      console.log(this.selectedTab);
    } else if (this.selectedTab === 'school') {
      console.log(this.selectedTab);
      this.loadSchool();
    } else if (this.selectedTab === 'user') {
      console.log(this.selectedTab);
      this.loadUser();
    } else if (this.selectedTab === 'board') {
      console.log(this.selectedTab);
      this.loadBoard();
    }  else if (this.selectedTab === 'access-control') {
      console.log(this.selectedTab);
      this.loadAccessControl();
    }

    console.log(value);
  }

  loadBoard(){
    if (this.adminContentLib) {
      this.adminContentLib.clear();
    }
    const componentFactory = this.componentFactoryResolver.resolveComponentFactory(
      SuperAdminBoardComponent
    );
    this.adminContentLib.createComponent(componentFactory);
  }
  loadSyncData() {
    if (this.adminContentLib) {
      this.adminContentLib.clear();
    }
    const componentFactory = this.componentFactoryResolver.resolveComponentFactory(
      SyncDataComponent
    );
    this.adminContentLib.createComponent(componentFactory);
  }

  loadQueueInfo() {
    if (this.adminContentLib) {
      this.adminContentLib.clear();
    }
    const componentFactory = this.componentFactoryResolver.resolveComponentFactory(
      QueueInfoComponent
    );
    this.adminContentLib.createComponent(componentFactory);
  }

  loadMyAccess() {
    if (this.adminContentLib) {
      this.adminContentLib.clear();
    }
    const componentFactory = this.componentFactoryResolver.resolveComponentFactory(
      MyAccessComponent
    );
    this.adminContentLib.createComponent(componentFactory);
  }

  loadAccessControl(){
    if (this.adminContentLib) {
      this.adminContentLib.clear();
    }
    const componentFactory = this.componentFactoryResolver.resolveComponentFactory(
      SuperAccessModuleComponent
    );
    this.adminContentLib.createComponent(componentFactory);
  }

  loadSchool() {
    if (this.adminContentLib) {
      this.adminContentLib.clear();
    }
    const componentFactory = this.componentFactoryResolver.resolveComponentFactory(
      SuperAdminSchoolComponent
    );
    this.adminContentLib.createComponent(componentFactory);
  }

  loadUser() {
    if (this.adminContentLib) {
      this.adminContentLib.clear();
    }
    const componentFactory = this.componentFactoryResolver.resolveComponentFactory(
      SuperAdminUserComponent
    );
    this.adminContentLib.createComponent(componentFactory);
  }
}
