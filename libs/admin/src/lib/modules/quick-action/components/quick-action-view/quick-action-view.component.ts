import { Component,  Input, OnInit, Output, EventEmitter } from '@angular/core';
import {
  faFighterJet,
  faUniversalAccess,
  faDatabase,
  faFileImport,
  faInfoCircle,
  faSearch
} from '@fortawesome/free-solid-svg-icons'; 
import { SubNavigation } from '../../../models/interfaces/dashboard-config.interface';
@Component({
  selector: 'standalone-ce6-admin-quick-action-view',
  templateUrl: './quick-action-view.component.html',
  styleUrls: ['./quick-action-view.component.scss']
})
export class QuickActionViewComponent implements OnInit {
  @Input() subNavigation: SubNavigation;
  @Output() tabValue = new EventEmitter();
  faFighterJet = faFighterJet;
  faUniversalAccess = faUniversalAccess;
  faDatabase = faDatabase;
  faFileImport = faFileImport;
  faInfoCircle = faInfoCircle;
  faSearch = faSearch;
  selectedTab = 'my-access';
  constructor() { }

  ngOnInit(): void {
  }

  switchTab(value) {
    this.selectedTab = value;
    this.tabValue.emit(this.selectedTab);
    // if (this.selectedTab === 'configuration') {
    //   console.log(this.selectedTab);
    // } else if (this.selectedTab === 'my-access') {
    //   console.log(this.selectedTab);
    // } else if (this.selectedTab === 'queue-info') {
    //   console.log(this.selectedTab);
    // } else if (this.selectedTab === 'sync-data') {
    //   console.log(this.selectedTab);
    // }

    console.log(value);
  }

}
