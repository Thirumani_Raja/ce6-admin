import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QuickActionViewComponent } from './quick-action-view.component';

describe('QuickActionViewComponent', () => {
  let component: QuickActionViewComponent;
  let fixture: ComponentFixture<QuickActionViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QuickActionViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QuickActionViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
