import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { QuickActionViewComponent } from './components/quick-action-view/quick-action-view.component';
import {
  NbButtonComponent,
  NbButtonModule,
  NbCardModule,
  NbCheckboxComponent,
  NbIconModule,
  NbSelectModule,
  NbInputModule,
  NbStepperModule
} from '@nebular/theme';



import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
@NgModule({
  declarations: [QuickActionViewComponent],
  imports: [
    CommonModule,
  NbButtonModule,
  NbCardModule,
  NbIconModule,
  NbSelectModule,
  NbInputModule,
  NbStepperModule,
  FontAwesomeModule
  ],
  exports: [QuickActionViewComponent],
  entryComponents: [QuickActionViewComponent]
})
export class QuickActionModule { }
