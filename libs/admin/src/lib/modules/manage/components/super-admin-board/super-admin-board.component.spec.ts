import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SuperAdminBoardComponent } from './super-admin-board.component';

describe('SuperAdminBoardComponent', () => {
  let component: SuperAdminBoardComponent;
  let fixture: ComponentFixture<SuperAdminBoardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SuperAdminBoardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SuperAdminBoardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
