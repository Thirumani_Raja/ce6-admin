import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'standalone-ce6-admin-super-admin-board',
  templateUrl: './super-admin-board.component.html',
  styleUrls: ['./super-admin-board.component.scss']
})
export class SuperAdminBoardComponent implements OnInit {
  options = [
    { value: 'Active', label: 'Active' },
    { value: 'Inactive', label: 'Inactive' },
    { value: 'All', label: 'All' }
  ];
  constructor() { }

  ngOnInit(): void {
  }

  addNewBoard() {
    this.openAddBoard(false, false);
  }

  protected openAddBoard(closeOnBackdropClick: boolean, closeOnEsc: boolean) {
    // this.dialogService.open(ManageBoardComponent, {
    //   closeOnBackdropClick,
    //   closeOnEsc
    // });
  }

}
