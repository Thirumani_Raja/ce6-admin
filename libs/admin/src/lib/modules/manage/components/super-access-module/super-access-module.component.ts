import { Component, OnInit } from '@angular/core';
import {
  faFighterJet,
  faUniversalAccess,
  faDatabase,
  faFileImport,
  faInfoCircle,
  faSearch,
  faSchool,
  faUsers,
  faFileSignature
} from '@fortawesome/free-solid-svg-icons';
@Component({
  selector: 'standalone-ce6-admin-super-access-module',
  templateUrl: './super-access-module.component.html',
  styleUrls: ['./super-access-module.component.scss']
})
export class SuperAccessModuleComponent implements OnInit {
  faFighterJet = faFighterJet;
  faUniversalAccess = faUniversalAccess;
  faDatabase = faDatabase;
  faFileImport = faFileImport;
  faInfoCircle = faInfoCircle;
  faSearch = faSearch;
  faSchool = faSchool;
  faUsers = faUsers;
  faFileSignature = faFileSignature;
  constructor() { }

  ngOnInit(): void {
  }

}
