import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SuperAccessModuleComponent } from './super-access-module.component';

describe('SuperAccessModuleComponent', () => {
  let component: SuperAccessModuleComponent;
  let fixture: ComponentFixture<SuperAccessModuleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SuperAccessModuleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SuperAccessModuleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
