import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'standalone-ce6-admin-super-admin-user',
  templateUrl: './super-admin-user.component.html',
  styleUrls: ['./super-admin-user.component.scss']
})
export class SuperAdminUserComponent implements OnInit {
  items = [
    { title: 'View' },
    { title: 'Edit' },
    { title: 'Reset Password' },
    { title: 'Edit Roles' },
    { title: 'Delete User' },
    { title: 'Version History' }
  ];

  options = [
    { value: 'Active', label: 'Active' },
    { value: 'Inactive', label: 'Inactive' },
    { value: 'All', label: 'All' }
  ];
  constructor() { }

  ngOnInit(): void {
  }

  createUser() {
    //this.openPopup();
  }
  // openPopup() {
  //   this.open(false, false);
  // }

  // protected open(closeOnBackdropClick: boolean, closeOnEsc: boolean) {
  //   this.dialogService.open(ManageUserComponent, {
  //     closeOnBackdropClick,
  //     closeOnEsc
  //   });
  //}


}
