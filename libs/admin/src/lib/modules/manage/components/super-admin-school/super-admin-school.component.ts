import { Component, OnInit } from '@angular/core';
import {
  faFighterJet,
  faUniversalAccess,
  faDatabase,
  faFileImport,
  faInfoCircle,
  faSearch,
  faSchool,
  faUsers,
  faFileSignature
} from '@fortawesome/free-solid-svg-icons';
@Component({
  selector: 'standalone-ce6-admin-super-admin-school',
  templateUrl: './super-admin-school.component.html',
  styleUrls: ['./super-admin-school.component.scss']
})
export class SuperAdminSchoolComponent implements OnInit {
  faFighterJet = faFighterJet;
  faUniversalAccess = faUniversalAccess;
  faDatabase = faDatabase;
  faFileImport = faFileImport;
  faInfoCircle = faInfoCircle;
  faSearch = faSearch;
  faSchool = faSchool;
  faUsers = faUsers;
  faFileSignature = faFileSignature;

  items = [
    { title: 'View' },
    { title: 'Edit' },
    { title: 'Reset Password' },
    { title: 'Edit Roles' },
    { title: 'Delete User' },
    { title: 'Version History' }
  ];
  options = [
    { value: 'Active', label: 'Active' },
    { value: 'Inactive', label: 'Inactive' },
    { value: 'All', label: 'All' }
  ];
  constructor() { }

  ngOnInit(): void {
  }

  addNewSchool(){}

}
