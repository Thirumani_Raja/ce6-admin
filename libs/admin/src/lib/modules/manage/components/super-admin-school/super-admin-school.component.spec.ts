import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SuperAdminSchoolComponent } from './super-admin-school.component';

describe('SuperAdminSchoolComponent', () => {
  let component: SuperAdminSchoolComponent;
  let fixture: ComponentFixture<SuperAdminSchoolComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SuperAdminSchoolComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SuperAdminSchoolComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
