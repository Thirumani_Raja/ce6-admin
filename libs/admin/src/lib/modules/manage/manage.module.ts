import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SuperAdminSchoolComponent } from './components/super-admin-school/super-admin-school.component';
import { SuperAdminUserComponent } from './components/super-admin-user/super-admin-user.component';
import { SuperAdminBoardComponent } from './components/super-admin-board/super-admin-board.component';
import { SuperAccessModuleComponent } from './components/super-access-module/super-access-module.component';
import { ManageViewComponent } from './components/manage-view/manage-view.component';
import {
  NbCardModule,
  NbIconModule,
  NbSelectModule,
  NbInputModule,
  NbStepperModule,
  NbButtonModule,
  NbContextMenuModule,
  NbRadioModule,
  NbCheckboxModule
} from '@nebular/theme';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { LibConfigService } from '../lib-config/services/lib-config.service';
import { LibConfigModule } from '../lib-config/lib-config.module';

@NgModule({
  declarations: [SuperAdminSchoolComponent, SuperAdminUserComponent, SuperAdminBoardComponent, SuperAccessModuleComponent, ManageViewComponent],
  imports: [
    CommonModule,
    NbCardModule,
    NbIconModule,
    NbSelectModule,
    NbInputModule,
    NbStepperModule,
    NbButtonModule,
    NbContextMenuModule,
    NbRadioModule,
    NbCheckboxModule,
    FontAwesomeModule,
  
    NbCardModule,
    NbIconModule,
    NbSelectModule,
    NbInputModule,
    NbStepperModule,
    NbButtonModule,
    FontAwesomeModule,
    NbContextMenuModule,
    NbRadioModule,
    NbCheckboxModule],
  exports: [SuperAdminSchoolComponent, SuperAdminUserComponent, SuperAdminBoardComponent, SuperAccessModuleComponent, ManageViewComponent],
  entryComponents: [SuperAdminSchoolComponent, SuperAdminUserComponent, SuperAdminBoardComponent, SuperAccessModuleComponent, ManageViewComponent]
})
export class ManageModule { }
