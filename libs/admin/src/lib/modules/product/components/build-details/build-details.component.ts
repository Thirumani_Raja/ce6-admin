import { Component, OnInit } from '@angular/core';
import { faDatabase } from '@fortawesome/free-solid-svg-icons';


@Component({
  selector: 'standalone-ce6-admin-build-details',
  templateUrl: './build-details.component.html',
  styleUrls: ['./build-details.component.scss']
})
export class BuildDetailsComponent implements OnInit {
  faDatabase = faDatabase;
  constructor() { }

  ngOnInit(): void {
  }

}
