import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BuildDetailsComponent } from './components/build-details/build-details.component';
import {
  NbCardModule,
  NbIconModule,
  NbSelectModule,
  NbInputModule,
  NbStepperModule,
  NbButtonModule,
  NbContextMenuModule,
  NbRadioModule,
  NbCheckboxModule
} from '@nebular/theme';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

@NgModule({
  declarations: [BuildDetailsComponent],
  imports: [
    CommonModule,
    NbCardModule,
  NbIconModule,
  NbSelectModule,
  NbInputModule,
  NbStepperModule,
  NbButtonModule,
  NbContextMenuModule,
  NbRadioModule,
  NbCheckboxModule,
  FontAwesomeModule
  ],
  exports: [BuildDetailsComponent],
  entryComponents: [BuildDetailsComponent]
})
export class ProductModule { }
