import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SupportViewComponent } from './components/support-view/support-view.component';

import {
  NbButtonModule,
  NbInputModule,
  NbLayoutModule,
  NbSelectModule
} from '@nebular/theme';
import { TicketListItemComponent } from './components/ticket-list-item/ticket-list-item.component';
import { CreateTicketFormComponent } from './components/create-ticket-form/create-ticket-form.component';
import { TicketDetailsViewComponent } from './components/ticket-details-view/ticket-details-view.component';
import { LibConfigService } from '../lib-config/services/lib-config.service';
import { LibConfigModule } from '../lib-config/lib-config.module';

@NgModule({
  declarations: [SupportViewComponent, TicketListItemComponent, CreateTicketFormComponent, TicketDetailsViewComponent],
  imports: [
    CommonModule,
    NbButtonModule,
    NbInputModule,
    NbLayoutModule,
    NbSelectModule,
    LibConfigModule.forChild(SupportViewComponent)
  ],
  exports: [SupportViewComponent, TicketListItemComponent, CreateTicketFormComponent, TicketDetailsViewComponent],
  entryComponents: [SupportViewComponent, TicketListItemComponent, CreateTicketFormComponent, TicketDetailsViewComponent]
})
export class SupportModule {}
