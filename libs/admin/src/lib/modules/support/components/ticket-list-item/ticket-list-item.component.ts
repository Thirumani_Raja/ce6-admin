import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'standalone-ce6-admin-ticket-list-item',
  templateUrl: './ticket-list-item.component.html',
  styleUrls: ['./ticket-list-item.component.scss']
})
export class TicketListItemComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
