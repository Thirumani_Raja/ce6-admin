import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'standalone-ce6-admin-support-view',
  templateUrl: './support-view.component.html',
  styleUrls: ['./support-view.component.scss']
})
export class SupportViewComponent implements OnInit {
  flag = 'form';
  constructor() { }

  ngOnInit(): void {
  }

  loadTicketDetails(value) {
    this.flag = value;
  }
  loadCreateForm(value) {
    this.flag = value;
  }
    

}
