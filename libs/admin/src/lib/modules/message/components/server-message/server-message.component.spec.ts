import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ServerMessageComponent } from './server-message.component';

describe('ServerMessageComponent', () => {
  let component: ServerMessageComponent;
  let fixture: ComponentFixture<ServerMessageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ServerMessageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ServerMessageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
