import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ServerMessageComponent } from './components/server-message/server-message.component';
import { ExceptionLogComponent } from './components/exception-log/exception-log.component';
import { MessageViewComponent } from './components/message-view/message-view.component';



@NgModule({
  declarations: [ServerMessageComponent, ExceptionLogComponent, MessageViewComponent],
  imports: [
    CommonModule
  ],
  exports: [ServerMessageComponent, ExceptionLogComponent, MessageViewComponent],
  entryComponents: [ServerMessageComponent, ExceptionLogComponent, MessageViewComponent]
})
export class MessageModule { }
