import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MyAccessComponent } from './components/my-access/my-access.component';
import { SyncDataComponent } from './components/sync-data/sync-data.component';
import { ConfigurationComponent } from './components/configuration/configuration.component';
import { QueueInfoComponent } from './components/queue-info/queue-info.component';
import {
  NbActionsModule,
  NbLayoutModule,
  NbCardModule,
  NbIconModule,
  NbSelectModule,
  NbButtonModule,
  NbInputModule,
  NbStepperModule
} from '@nebular/theme';
import { OverviewViewComponent } from './components/overview-view/overview-view.component';


@NgModule({
  declarations: [MyAccessComponent, SyncDataComponent, ConfigurationComponent, QueueInfoComponent, OverviewViewComponent],
  imports: [
    CommonModule,NbActionsModule,
    NbLayoutModule,
    NbCardModule,
    NbIconModule,
    NbSelectModule,
    NbButtonModule,
    NbInputModule,
    NbStepperModule
  ],
  exports: [MyAccessComponent, SyncDataComponent, ConfigurationComponent, QueueInfoComponent, OverviewViewComponent],
  entryComponents: [MyAccessComponent, SyncDataComponent, ConfigurationComponent, QueueInfoComponent, OverviewViewComponent]
})
export class OverViewModule { }
