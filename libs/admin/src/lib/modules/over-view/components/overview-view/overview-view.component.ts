import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'standalone-ce6-admin-overview-view',
  templateUrl: './overview-view.component.html',
  styleUrls: ['./overview-view.component.scss']
})
export class OverviewViewComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
