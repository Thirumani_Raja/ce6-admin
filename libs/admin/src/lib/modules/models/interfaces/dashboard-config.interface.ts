export interface SubNavigation {
    id: string;
    name: string;
  }
  
  export interface Navigation {
    id: string;
    name: string;
    subnavigation: SubNavigation[];
  }
  
  export interface AdminRoles {
    id: string;
    role: string;
    navigation: Navigation[];
  }
  