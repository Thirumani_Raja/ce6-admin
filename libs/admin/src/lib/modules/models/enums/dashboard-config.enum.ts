export enum adminroles {
    SUPER_ADMIN = 'super-admin',
    SITE_ADMIN = 'site-admin',
    SCHOOL_ADMIN = 'school-admin',
    PRINCIPAL = 'principal',
    TEACHER = 'teacher'
  }
  