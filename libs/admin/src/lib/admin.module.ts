import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NbThemeModule } from '@nebular/theme';
import { AdminViewComponent } from './components/admin-view/admin-view.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
// import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule } from '@angular/forms';
import {
  NbActionsModule,
  NbLayoutModule,
  NbCardModule,
  NbIconModule,
  NbSelectModule,
  NbButtonModule,
  NbInputModule,
  NbStepperModule
} from '@nebular/theme';
import { QuickActionModule } from './modules/quick-action/quick-action.module';
import { DynamicComponentManifest, LibConfigModule } from './modules/lib-config/lib-config.module';
const manifests: DynamicComponentManifest[] = [
  {
    componentId: 'support',
    path: 'support',
    loadChildren: () =>
      import('./modules/support/support.module').then(m => m.SupportModule)
  },
  {
    componentId: 'manage',
    path: 'manage',
    loadChildren: () =>
      import('./modules/manage/manage.module').then(m => m.ManageModule)
  },
  {
    componentId: 'message',
    path: 'message',
    loadChildren: () =>
      import('./modules/message/message.module').then(m => m.MessageModule)
  },

 
  {
    componentId: 'overview',
    path: 'overview',
    loadChildren: () =>
      import('./modules/over-view/over-view.module').then(
        m => m.OverViewModule
      )
  },
  {
    componentId: 'product',
    path: 'product',
    loadChildren: () =>
      import('./modules/product/product.module').then(m => m.ProductModule)
  }
];
@NgModule({
  imports: [CommonModule,NbThemeModule.forRoot(), 
    LibConfigModule.forRoot(manifests),
     NbActionsModule,
     FormsModule,
    NbLayoutModule,
    NbCardModule,
    NbIconModule,
    NbSelectModule,
    NbButtonModule,
    NbInputModule,
    QuickActionModule,
    NbStepperModule,FontAwesomeModule,
    LibConfigModule.forChild(AdminViewComponent)
  ],
  declarations: [AdminViewComponent],
  exports: [AdminViewComponent],

  entryComponents: [AdminViewComponent]
})
export class AdminModule {}
