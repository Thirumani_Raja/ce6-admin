import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
import { adminroles } from '../modules/models/enums/dashboard-config.enum';
import { AdminRoles, Navigation, SubNavigation } from '../modules/models/interfaces/dashboard-config.interface';

@Injectable({
  providedIn: 'root'
})
export class AdminService {

  private userRoles = new BehaviorSubject<string>(adminroles.SUPER_ADMIN);
  public userRoles$ = this.userRoles.asObservable();

  private roleNavigation = new Subject<Navigation>();
  public roleNavigation$ = this.roleNavigation.asObservable();

  private roleSubNavigation = new Subject<SubNavigation>();
  public roleSubNavigation$ = this.roleSubNavigation.asObservable();

  constructor(private http: HttpClient) {
    this.getUserRoles().subscribe(data => {
      console.log('userRoles', data.userRoles);
    });
  }
  getUserRoles(): Observable<any> {
    return this.http.get('assets/config.json');
  }

  loadAdminConfig(role: AdminRoles) {
    this.userRoles.next(role.id);
    let navigation;
    navigation = role.navigation;
    this.loadNavigation(navigation);
    let subnavigation;
    subnavigation = role.navigation[0].subnavigation;
    this.loadSubNavigation(subnavigation);
    console.log(' current Data', role);
  }

  loadNavigation(navigation: Navigation) {
    console.log('navigation service', navigation);

    this.roleNavigation.next(navigation);
  }

  loadSubNavigation(subnav: SubNavigation) {
    console.log('sub-navigation service', subnav);
    this.roleSubNavigation.next(subnav);
  }
}
