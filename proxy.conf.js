const PROXY_CONFIG = [
    {
      context: ['/tce-auth-api', '/tce-teach-api', '/tce-repo-api'],
      target: 'http://172.18.1.57:8080',
      secure: false,
      onProxyRes: function(proxyRes, req, res) {
        proxyRes.headers['www-authenticate'] = 'none';
      }
    }
  ];
  
  module.exports = PROXY_CONFIG;
  