// export const environment = {
//   production: true
// };
import { ProductionEnvironmentBaseline } from '../app/models/environments/environment.production.baseline';
import * as merge from 'deepmerge';

export const environment = merge(ProductionEnvironmentBaseline, {});
