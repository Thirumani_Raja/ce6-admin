import { Injectable } from '@angular/core';
import {
  HttpClient,
  HttpHeaders
} from '@angular/common/http';
import {
  ClientID,
  SignInState,
  RefreshToken
} from '../models/login/login.interface';
import {
  AuthStateEnum,
  FirstTimeUserStatusEnum,
  UserState,
  UserStatusEnum
} from '../models/auth/auth.interface';
import { ReplaySubject, BehaviorSubject } from 'rxjs';
export interface LoginData {
  userName: string;
  password: string;
  loginType: string;
}
import {take} from 'rxjs/operators'
import { RequestApiService } from './request-api.service';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  
  private userStatus: number;
  private _defaultSignInState = SignInState.LOGIN_PASSWORD;
  private _currentSignInState = this._defaultSignInState;

  private _userState: UserState = {
    authStatus: AuthStateEnum.PRE_AUTH,
    previousAuthStatus: null,
    firstTimeUserStatus: FirstTimeUserStatusEnum.NOT_DETERMINED
  };

  private _authFetching = false;
  private authFetching = new ReplaySubject<boolean>(1);
  public authFetching$ = this.authFetching.asObservable();

  private _tokenData: RefreshToken;
  private tokenData = new ReplaySubject<RefreshToken>(1);
  public $tokenData = this.tokenData.asObservable();

  private authError = new ReplaySubject<string>(1);
  public authError$ = this.authError.asObservable();

  private currentSignInState = new ReplaySubject<SignInState>(1);
  public currentSignInState$ = this.currentSignInState.asObservable();

  private userState = new BehaviorSubject<UserState>(this._userState);
  public userState$ = this.userState.asObservable();
  private organizationName = 'tcdeli-lxplm#';
  /** TODO: make organization dynamic*/
  // private organization = 'classedge-school';
  // private organization = '';
  private grant_type = 'password';
  httpPostOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/x-www-form-urlencoded'
    })
  };

  private autoSignOutTimeout = null;
  private _clientIdRetrieved = new ReplaySubject<any>(1);

  private clientIdStorageName = 'clientIdExpirationDate';
  private sessionTimeoutName = 'sessionTimeoutKey';

  

  constructor( private http: HttpClient,
    private requestApiService: RequestApiService) { 
      this.getOrganizations('init-contructor');
    }

    private generateClientIDVersion(callee: string): Promise<any> {
      return new Promise((resolve, reject) => {
        this.http
          .get<ClientID>(this.requestApiService.getUrl('clientId'))
          .subscribe(clientID => {
            console.log('TCE: AuthenticationService -> clientID', clientID);
  
            if (clientID.sessionTimeout) {
              const sessionTimeout = parseInt(clientID.sessionTimeout, 10);
              console.log( 'TCE: AuthenticationService -> generateClientIDVersion -> clientID',  clientID  );
  
              //const dateOfExpiration = dayjs().add(sessionTimeout, 'second');
              // sessionStorage.setItem(
              //   this.clientIdStorageName,
              //   //'' + dateOfExpiration.getTime()
              //   dateOfExpiration.toISOString()
              // );
              sessionStorage.setItem(
                this.sessionTimeoutName,
                clientID.sessionTimeout
              );
  
              // console.log('TCE: AuthenticationService -> generateClientIDVersion -> sessionTimeout', sessionTimeout);
              // this.setSessionTimeout(sessionTimeout * 1000, callee + " > generateClientIDVersion");
            }
            this.requestApiService.versionId = clientID.apiVersion;
            this._clientIdRetrieved.next();
  
            resolve(clientID);
          });
      });
    }

    setTokenData(tokenData: RefreshToken, callee: string) {
      console.log('TCE: AuthenticationService -> setTokenData -> callee', callee);
      this._tokenData = tokenData;
      this.tokenData.next(tokenData);
      //console.log('tokenData-->> ', tokenData);
      if (tokenData) {
        sessionStorage.setItem('token', tokenData.access_token);
        sessionStorage.setItem('refreshToken', tokenData.refresh_token);
        const myDate = new Date(
          new Date().getTime() + tokenData.expires_in * 1000
        );
        document.cookie =
          'access_token=' +
          tokenData.access_token +
          ';expires=' +
          myDate.toUTCString() +
          ';path=/tce-repo-api/1/web/1/content/fileservice';
        //this.setRefreshTokenInterval('setTokenData');
        if (this._tokenData.loginstatus !== undefined) {
          this.setUserStatus(this._tokenData.loginstatus);
          this.setUserState({
            authStatus: AuthStateEnum.AUTH,
            firstTimeUserStatus:
              this._tokenData.loginstatus !== UserStatusEnum.ACTIVE_USER
                ? FirstTimeUserStatusEnum.FIRST_TIME
                : FirstTimeUserStatusEnum.NOT_FIRST_TIME
          });
        }
  
        /**
         * tce: Need to check the functionality
         */
        /* if (!this.sessionTimeoutService.idleIsSet) {
          this.sessionTimeoutService.makeSessionTimeout("!idleIsSet");
        } */
      } else {
        sessionStorage.removeItem('token');
        sessionStorage.removeItem('refreshToken');
      }
    }

    setUserState(newState: UserState) {
      this._userState = {
        ...this._userState,
        ...newState,
        ...{
          previousAuthStatus: this._userState.authStatus
        }
      };
      this.userState.next(this._userState);
    }

    loginPWD(value: LoginData, callee: string) {
      console.log('TCE: AuthenticationService -> loginPWD -> callee', callee,value);
      this.getOrganizations('login-PWD');
      let userName = '';
      let password = '';
      if (value.loginType === 'pin') {
        userName = value.userName;
        password = value.password;
      } else {
        userName = value.userName;
        password = value.password;
        if (userName !== 'dev.admin') {
          console.log( 'TCE: AuthenticationService -> loginPWD -> this.organizationName',this.organizationName);
          userName = this.organizationName  + userName;
        }
      }
  
      const body = `username=${userName}&password=${password}&grant_type=${this.grant_type}`;
  
      this.onBeforeAuthFetch();
      /**
       * Taken to top was in bootom as the sequence of token and client was wrong
       * :: clientid is to be called before token call
       */
  
      /**
       * Used a promise fun to wait for the response to come before login fires.
       */
      this.generateClientIDVersion('login-PWD').then(promisedData => {
        console.log('TCE: AuthenticationService -> loginPWD -> promisedData', promisedData);
  
        this._clientIdRetrieved.pipe(take(1)).subscribe(() => {
          this.http
            .post<RefreshToken>(
              this.requestApiService.getUrl('signIn'),
              body,
              this.httpPostOptions
            )
            .subscribe(
              (tokenData: RefreshToken) => {
                console.log('login tokenData-->', tokenData);
                console.log('login value', value.loginType);
                this.setTokenData(tokenData, 'loginPWD');
  
                if (value.loginType === 'password') {
                  console.log('login value if', value.loginType);
                  switch (tokenData.loginstatus) {
                    case UserStatusEnum.ACTIVE_USER:
                      this.setCurrentSignInState(SignInState.DESTROY);
                      //console.log('case-->', UserStatusEnum.CHANGE_PASSWORD);
                      break;
                    case UserStatusEnum.CHANGE_PASSWORD:
                      this.setCurrentSignInState(SignInState.CHANGE_PASSWORD);
                      //console.log('case-->', UserStatusEnum.CHANGE_PASSWORD);
                      break;
                    case UserStatusEnum.CHANGE_PIN:
                      this.setCurrentSignInState(SignInState.CHANGE_PIN);
                      break;
                    case UserStatusEnum.NEW_USER:
                      this.setCurrentSignInState(SignInState.CHANGE_PASSWORD);
                      //console.log('case-->new user', UserStatusEnum.NEW_USER);
                      //this.router.navigate(['/login/changepassword']);
                      break;
                    default:
                      break;
                  }
                } else {
                  //console.log('login else value', value.loginType);
                  this.setCurrentSignInState(SignInState.DESTROY);
                  //this.router.navigate(['/' + postLoginRoute]);
                }
                this.onAfterAuthFetch();
              },
              error => {
                this.onAfterAuthFetch();
                if (value.loginType === 'password') {
                  this.setAuthError('Incorrect User ID or Password');
                } else if (value.loginType === 'pin') {
                  //console.log('login value ele', value.loginType);
                  this.setAuthError('Incorrect PIN');
                }
  
                //console.log('Error logging in!', error);
              }
            );
        });
      });
    }

    onBeforeAuthFetch() {
      console.log("onBeforeAuthFetch")
      this.setAuthFetching(true);
      this.setAuthError('');
    }
  
    setAuthError(errorMessage: string) {
      console.log("setAuthError",errorMessage)
      this.authError.next(errorMessage);
    }
  
    setAuthFetching(isFetching: boolean) {
      console.log("setAuthFetching++++++++++++++++++++++++++++++++++++++++++++++++++++++++++",isFetching,this._authFetching)
      if (isFetching !== this._authFetching) {
        this._authFetching = isFetching;
        this.authFetching.next(this._authFetching);
      }
    }

    onAfterAuthFetch() {
      console.log("onAfterAuthFetch")
      this.setAuthFetching(false);
    }

getOrganizations(callee: string) {
  console.log( 'TCE: AuthenticationService -> getOrganizations -> callee',callee, this.requestApiService.getUrl('organizations'));
  this.http
    .get<any>(this.requestApiService.getUrl('organizations'))
    .subscribe(
      data => {
        console.log( 'TCE: AuthenticationService -> getOrganizations -> data',  data );
        /** TODO: set it after school list comes from mel */
        this.organizationName = data.suggestions[0];
        console.log( 'TCE: AuthenticationService -> getOrganizations -> this.organizationName', this.organizationName );
      },
      err => {
        console.log("getOrganizations err-------------->",err)
      }
    );
}

setUserStatus(value: number) {
  console.log("setUserStatus",value)
  this.userStatus = value;
}

getUserStatus() {
  console.log("getUserStatus",this.userState)
  return this.userStatus;
}
setCurrentSignInState(state: SignInState) {
  console.log("setCurrentSignInState",state)
  let nextState = state;
  if (nextState === SignInState.SESSION_DEFAULT) {
    nextState = this._defaultSignInState;
  }
  this._currentSignInState = nextState;
  this.currentSignInState.next(this._currentSignInState);
}
}
