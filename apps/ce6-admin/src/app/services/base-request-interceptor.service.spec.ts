import { TestBed } from '@angular/core/testing';

import { BaseRequestInterceptorService } from './base-request-interceptor.service';

describe('BaseRequestInterceptorService', () => {
  let service: BaseRequestInterceptorService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(BaseRequestInterceptorService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
