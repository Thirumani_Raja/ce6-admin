import { Injectable, Inject } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpInterceptor,
  HttpHeaders,
  HttpErrorResponse,
  HttpEventType
} from '@angular/common/http';
import { tap, catchError, delay, mergeMap } from 'rxjs/operators';
import { interval, throwError } from 'rxjs';
import { EnvironmentConfig } from '../models/environments/environment.interface';

@Injectable({
  providedIn: 'root'
})
export class BaseRequestInterceptorService implements HttpInterceptor{

  constructor(@Inject('environment') private environment: EnvironmentConfig) { }
  intercept(request: HttpRequest<any>, next: HttpHandler) {
    console.log(request)
    console.log("intercept")
    const mockHttpErrorResponse: HttpErrorResponse = {
      error: {
        error: 'invalid_token',
        message: 'HTTP Error!'
      },
      url: request.url,
      status: 404,
      statusText: 'Forbidden',
      message: 'Error Getting Item',
      name: 'HttpErrorResponse',
      ok: false,
      type: HttpEventType.Response,
      headers: new HttpHeaders()
    };

    const token = sessionStorage.getItem('token');
    /*if (
      token &&
      !(request.url.split('/').pop() === 'token') &&
      (!request.url.includes('/sso') || !request.url.includes('/0') || request.url.includes('revoke'))
    )*/

    if (
      (!request.url.includes('/sso') && !request.url.includes('/0')) ||
      request.url.includes('revoke')
    ) {
      request = request.clone({
        headers: new HttpHeaders({
          Authorization: 'Bearer ' + token
        }),
        withCredentials: true
      });
    } else {
      request = request.clone({
        withCredentials: true
      });
    }

    if (request.url.startsWith('/') && !request.url.startsWith('//')) {
      const bases = document.getElementsByTagName('base');
      let baseHref: string = null;

      if (bases.length > 0) {
        baseHref = bases[0].href;
      }
      if (baseHref && baseHref !== '/') {
        if (baseHref.endsWith('/')) {
          baseHref = baseHref.substring(0, baseHref.length - 1);
        }
        request = request.clone({
          url: baseHref + request.url
        });
      }
    }

    return next.handle(request).pipe(
      delay(
        request.url.endsWith('.svg') || request.url.includes('assets/')
          ? 0
          : this.environment.api.globalResponseDelay
      ),
      tap(res => {
        if (this.environment.api.manualErrorTriggerEndpoints) {
          this.environment.api.manualErrorTriggerEndpoints.forEach(value => {
            if (request.url.includes(value)) {
              throw new HttpErrorResponse(mockHttpErrorResponse);
            }
          });
        }
      }),
      catchError(err => {
        console.log(err)
        return interval(
          request.url.endsWith('.svg')
            ? 0
            : this.environment.api.globalResponseDelay
        ).pipe(
          mergeMap(() => {
            if (err.error.error === 'invalid_token' || err.status === 401) {
              //this.store.dispatch(new RefreshTokenAction());
            }
            return throwError(err);
          })
        );
      })
    );
  }
}
