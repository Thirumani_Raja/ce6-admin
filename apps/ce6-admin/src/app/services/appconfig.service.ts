import { HttpClient } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';

import { EnvironmentConfig } from '../models/environments/environment.interface';

@Injectable({
  providedIn: 'root'
})
export class AppconfigService {
  href: any;
  private _config: any = null;
  // Path to your config file
  private CONFIG_FILE_PATH = this.environment.configFile;

  constructor(
    private http: HttpClient,
    @Inject('environment') private environment: EnvironmentConfig
  ) { }
  public load() {
    const urlParams = new URLSearchParams(window.location.search);
    return new Promise((resolve, reject) => {
      this.http
        .get(this.CONFIG_FILE_PATH)
        .pipe(
          map((res: any) => {
            console.log("res",res,this.CONFIG_FILE_PATH)
            /* If using the mockdata environment, turn off the login and intro, because it's annoying for developers. */
            if (this.environment.mockdata) {
              res.global_setting.modulelogin = 0;
              res.global_setting.moduleintro = 0;
            }
            this.href = window.location.href;
            if (this.href) {
              const letterNumber = /^[0-9]+$/;
              const newhref = this.href.split('?').pop();
              if (newhref) {
                const newconfigvalue = newhref
                  .split('&')
                  .map(pair => pair.split('='));

                const paramArr = {
                  pb: { name: 'playerbehaviour' },
                  wb: { name: 'modulewhiteboard' },
                  anim: { name: 'animation' }
                };
                newconfigvalue.forEach(([key, value]) => {
                  Object.entries(paramArr).map(([newkey, newvalue]) => {
                    if (key === newkey) {
                      key = newvalue.name;
                    }
                  });
                  if (value && value.match(letterNumber)) {
                    res.global_setting[key] = parseInt(value);
                  } else {
                    res.global_setting[key] = value;
                  }
                });
              }
            }
            return res;
          }),
          catchError(error => throwError(error))
        )
        .subscribe(confRes => {
          this._config = confRes;
          resolve(true);
        });
    });
  }
}
