/**
 * Interface definition for question
 *
 * @export
 * @interface ILogin
 */

export enum SignInState {
    LOGIN_PASSWORD = 'auth.login.password',
    LOGIN_PIN = 'auth.login.pin',
    CHANGE_PASSWORD = 'auth.change.password',
    CHANGE_PIN = 'auth.change.pin',
    DESTROY = 'auth.destroy',
    SESSION_DEFAULT = 'session_default'
  }
  
  export enum SignInModuleState {
    ACTIVE = 'active',
    INACTIVE = 'inactive'
  }
  
export interface ILogin {
    access_token: string;
    expires_in: number;
    loginstatus: number;
    refresh_token: string;
    scope: string;
    token_type: string;
  }
  export interface ClientID {
    apiVersion: string;
    sessionTimeout: string;
    defaultSchool: string;
  }
  
  export interface RefreshToken {
    access_token: string;
    token_type: string;
    refresh_token: string;
    expires_in: number;
    scope: string;
    loginstatus: number;
  }
  export interface ExtendSession {
    access_token: string;
    token_type: string;
    refresh_token: string;
    expires_in: number;
    scope: string;
    loginstatus: number;
  }
  
  export interface CheckPin {
    orgs: string[];
    userName: string;
  }
  
  export interface LoginData {
    userName: string;
    password: string;
    loginType: string;
  }
  
  export interface ChangePassword {
    success: string;
  }
  
  export interface AutoGeneratePin {
    userId: null;
    pin: string;
    partition: number;
  }
  
  export interface ApiUser {
    userId: string;
    organizationId: string;
    sessionKeys: Object;
    type: string;
    data?: string;
  }
  
  export class User {
    userId: string;
    organizationId: string;
    sessionKeys: Object;
    type: string;
    recentClassesData: Object[] = [];
  
    constructor(apiResponse: ApiUser, callee: string) {
      this.userId = apiResponse.userId;
      this.organizationId = apiResponse.organizationId;
      this.sessionKeys = apiResponse.sessionKeys;
      this.type = apiResponse.type;
  
      if (apiResponse.data) {
        try {
          this.recentClassesData = JSON.parse(apiResponse.data);
        } catch (e) {
          console.error('Error parsing recent class data in user!', e);
        }
      }
    }
  }
  