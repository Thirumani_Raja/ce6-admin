export enum AuthStateEnum {
    PRE_AUTH = 'preAuthentication',
    NO_AUTH = 'notAuthenticated',
    AUTH = 'authenticated'
  }
  
  export enum UserStatusEnum {
    ACTIVE_USER = 1,
    CHANGE_PASSWORD = 3,
    CHANGE_PIN = 5,
    NEW_USER = 7
  }
  
  export enum FirstTimeUserStatusEnum {
    NOT_DETERMINED = 0,
    FIRST_TIME = 1,
    NOT_FIRST_TIME = 2
  }
  
  export interface UserState {
    authStatus?: AuthStateEnum;
    firstTimeUserStatus?: FirstTimeUserStatusEnum;
    previousAuthStatus?: AuthStateEnum;
  }
  