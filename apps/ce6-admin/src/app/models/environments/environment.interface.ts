/* These interfaces inform base level environment configuration for individual apps. If there is a requirement for additional properties that
are APP SPECIFIC, there should be specific interfaces inside of that app that extend these to define the app specific interfaces. */

export interface EnvironmentConfig {
    coreLibVersion: any;
    production: boolean;
    mockdata: boolean;
    api: EnvironmentApiConfig;
    configFile?: string;
  }
  
  export interface EnvironmentApiConfig {
    globalResponseDelay: number;
    manualErrorTriggerEndpoints?: string[];
    baseUrls: {
      auth: string;
      general: string;
      file: string;
      fileupload: string;
    };
    
    clientId: EnvironmentApiUrl;
    signIn: EnvironmentApiUrl;
    refreshToken: EnvironmentApiUrl;
    extendSession: EnvironmentApiUrl;
    organizations: EnvironmentApiUrl;
    logout: EnvironmentApiUrl;
    getQuestionBank: EnvironmentApiUrl;
    addQuestion: EnvironmentApiUrl;
    editQuestion: EnvironmentApiUrl;
    deleteQuestion: EnvironmentApiUrl;
  }
  
  export interface EnvironmentApiUrl {
    url: string;
    baseUrlType: EnvironmentApiBaseUrlType;
  }
  
  export enum EnvironmentApiBaseUrlType {
    AUTH = 'auth',
    GENERAL = 'general',
    FILE = 'file',
    FILEUPLOAD = 'fileupload'
  }
  