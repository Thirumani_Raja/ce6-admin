import {
    EnvironmentApiBaseUrlType,
    EnvironmentConfig
  } from './environment.interface';
  
  /*
  Baseline implementation of app-specific Environment Configuration.
  */
  export const EnvironmentBaseline: EnvironmentConfig = {
    coreLibVersion: "0.7.0",
    production: false,
    mockdata: false,
    configFile: '/assets/config.json',
    // earlier base url has phyical url + api, now it is '/api-name' only
    // this change will allow resolving relative api path
    api: {
      globalResponseDelay: 0,
      baseUrls: {
        auth: '../tce-auth-api',
        general: '../tce-teach-api',
        file: '../tce-repo-api',
        fileupload: '../tce-repo-api'
      },
      // auth endpoints   
      clientId: {
        url: '/0/api/@apiVersion@/sso/clientid',
        baseUrlType: EnvironmentApiBaseUrlType.AUTH
      },
      signIn: {
        url: '/0/api/@apiVersion@/sso/token',
        baseUrlType: EnvironmentApiBaseUrlType.AUTH
      },
      refreshToken: {
        url: '/0/api/@apiVersion@/sso/token',
        baseUrlType: EnvironmentApiBaseUrlType.AUTH
      },
      /**
       * TODO:add in mockdata also
       */
      extendSession: {
        url: '/1/api/@apiVersion@/sso/extend',
        baseUrlType: EnvironmentApiBaseUrlType.AUTH
      },
      organizations: {
        url: '/0/api/@apiVersion@/admin/organizations/tcde',
        baseUrlType: EnvironmentApiBaseUrlType.AUTH
      },
     
      logout: {
        url: '/1/api/@apiVersion@/sso/revoke/@currentRefreshToken@',
        baseUrlType: EnvironmentApiBaseUrlType.AUTH
      },
      //QB endpoints
      getQuestionBank: {
        url: '/1/api/@apiVersion@/serve/custom/qb',
        baseUrlType: EnvironmentApiBaseUrlType.GENERAL
      },
      addQuestion: {
        url: '/1/api/@apiVersion@/serve/custom/qb',
        baseUrlType: EnvironmentApiBaseUrlType.GENERAL
      },
      editQuestion: {
        url: '/1/api/@apiVersion@/serve/custom/qb',
        baseUrlType: EnvironmentApiBaseUrlType.GENERAL
      },
      deleteQuestion: {
        url: '/1/api/@apiVersion@/serve/custom/qb/@qbId@',
        baseUrlType: EnvironmentApiBaseUrlType.GENERAL
      }
    }
  };
  