import { EnvironmentBaseline } from './environment.baseline';
import { EnvironmentConfig } from './environment.interface';
import * as merge from 'deepmerge';

/*
Baseline implementation of app-specific Production Environment Configuration. Deep merged from the baseline environment file.
*/
export const ProductionEnvironmentBaseline: EnvironmentConfig = merge(
  EnvironmentBaseline,
  {
    production: true
  }
);
