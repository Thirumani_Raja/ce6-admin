import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { AppconfigService } from '../services/appconfig.service';
import { AuthenticationService } from '../services/authentication.service';
import { AuthStateEnum } from '../models/auth/auth.interface';

@Injectable()
export class DefaultRouteGuard implements CanActivate {
  isLoggedIn = false;
  constructor(
    private router: Router,
    private appConfigService: AppconfigService,
    private authService: AuthenticationService
  ) {
    this.authService.userState$.subscribe(
      authState =>
        (this.isLoggedIn = authState.authStatus === AuthStateEnum.AUTH)
    );
  }

  canActivate(): boolean {
    let routePath = '';
    if (this.isLoggedIn) {
    } else {
      routePath = '/auth';
    }

    if (routePath) {
      this.router.navigate([routePath]);
    } else {
      return true;
    }
  }
}
