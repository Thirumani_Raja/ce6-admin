import { Component } from '@angular/core';

@Component({
  selector: 'standalone-ce6-admin-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'ce6-admin';
}
