import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DefaultRouteGuard } from './guards/default-route.guard';
import { AuthenticationComponent } from './features/auth/components/authentication/authentication.component';

const route: Routes = [
  {
    path: '',
    component: AuthenticationComponent
  }]

const routes: Routes = [
    {
        path: 'auth',
        loadChildren: () =>
          import('../app/features/auth/auth.module').then(
            mod => mod.AuthModule
          )
      },
     
      
      {
        path: '',
        canActivate: [DefaultRouteGuard],
        children: []
      }
    
  
];

@NgModule({
  imports: [RouterModule.forRoot(route)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
