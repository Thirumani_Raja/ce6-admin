import { Component, OnInit, ViewChild, ViewContainerRef } from '@angular/core';
import { AuthenticationService } from 'apps/ce6-admin/src/app/services/authentication.service';
import { AdminViewComponent } from 'libs/admin/src/lib/components/admin-view/admin-view.component';
import { LibConfigService } from 'libs/admin/src/lib/modules/lib-config/services/lib-config.service';

@Component({
  selector: 'standalone-ce6-admin-authentication',
  templateUrl: './authentication.component.html',
  styleUrls: ['./authentication.component.scss']
})
export class AuthenticationComponent implements OnInit {
  @ViewChild('adminContentLib', { static: true, read: ViewContainerRef })
  adminContentLib: ViewContainerRef | undefined;
  constructor(private authenticationService : AuthenticationService, private libConfigService : LibConfigService ) { }

  ngOnInit(): void {
    this.openAdmin();
    //uncomment loadAdmin method for with out auth admin loading
    //this.loadAdmin(); 
  }

  openAdmin(){
    this.onLoginSubmit();

    this.authenticationService.authFetching$.subscribe(auth=>{
      if(auth){
       // console.log("on Auth check",auth);
        this.loadAdmin();

      }
    })

    

  }

  loadAdmin() {
    if (this.adminContentLib) {
      this.adminContentLib.clear();
    }
    this.libConfigService
      .getComponentFactory<AdminViewComponent>('admin')
      .subscribe({
        next: componentFactory => {
          if (!this.adminContentLib) {
            return;
          }
          const ref = this.adminContentLib.createComponent(componentFactory);
        },
        error: err => {
          console.warn(err);
        }
      });
  }

  onLoginSubmit() {
    //login api
    // Currently waiting to hear back about the correct password and the domain on which this
    // is to be run.
    const loginDetails = {
      userName: "school.admin",
      password: "123",
      loginType: 'password'
    };
    this.authenticationService.loginPWD(loginDetails, 'onLogin-Submit');
  }

  // loadAdminView() {
  //   this.libConfigService
  //     .getComponentFactory<admin>('admin')
  //     .subscribe({
  //       next: componentFactory => {
  //         if (!this.qb) {
  //           return;
  //         }
  //         const ref = this.qb.createComponent(componentFactory);
  //         ref.changeDetectorRef.detectChanges();
         
  //       },
  //       error: err => {
  //         console.warn(err);
  //       }
  //     });
  // }

}
