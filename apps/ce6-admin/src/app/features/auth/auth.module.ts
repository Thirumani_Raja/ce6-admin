import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuthenticationComponent } from './components/authentication/authentication.component';
import {AuthRoutingModule} from './auth-routing.module';
import {AdminModule} from '../../../../../../libs/admin/src/lib/admin.module';
import { DynamicComponentManifest, LibConfigModule } from 'libs/admin/src/lib/modules/lib-config/lib-config.module';

const manifests: DynamicComponentManifest[] = [
  {
    componentId: 'admin',
    path: 'admin',
    loadChildren: () =>
      import('../../../../../../libs/admin/src/lib/admin.module').then(m => m.AdminModule)
  }]

@NgModule({
  declarations: [AuthenticationComponent],
  imports: [
    CommonModule,
    AuthRoutingModule,
    AdminModule,
    LibConfigModule.forRoot(manifests)

  ],
  exports: [AuthenticationComponent],
  entryComponents: [AuthenticationComponent]
})
export class AuthModule { }
