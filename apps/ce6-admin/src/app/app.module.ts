import { BrowserModule } from '@angular/platform-browser';
import { APP_INITIALIZER, NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { DefaultRouteGuard } from './guards/default-route.guard';

import { AppComponent } from './app.component';
import { AuthenticationService } from './services/authentication.service';
import { AppconfigService } from './services/appconfig.service';
import { environment } from '../environments/environment';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { BaseRequestInterceptorService } from './services/base-request-interceptor.service';
import { AuthModule } from './features/auth/auth.module';
import { OverViewModule } from '../../../../libs/admin/src/lib/modules/over-view/over-view.module';
import { from } from 'rxjs';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { NbMenuService, NbThemeModule } from '@nebular/theme';

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    AuthModule,
    HttpClientModule,
    OverViewModule,
    FontAwesomeModule,
    NbThemeModule.forRoot()
  ],
  providers: [
    DefaultRouteGuard,
    AuthenticationService,
    NbMenuService,
    {
      provide: APP_INITIALIZER,
      // Config loader factory
      // Note: This service will load the config file upon application load
      useFactory: (config: AppconfigService) => () => config.load(),
      deps: [AppconfigService],
      multi: true
    },
    {
      provide: 'environment',
      useValue: environment
    },

    [
      {
        provide: HTTP_INTERCEPTORS,
        useClass: BaseRequestInterceptorService,
        multi: true
      }
    ]
  ],
  bootstrap: [AppComponent],
  exports: [],
  entryComponents: []
})
export class AppModule {}
