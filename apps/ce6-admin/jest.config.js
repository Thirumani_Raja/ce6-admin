module.exports = {
  name: 'ce6-admin',
  preset: '../../jest.config.js',
  coverageDirectory: '../../coverage/apps/ce6-admin',
  snapshotSerializers: [
    'jest-preset-angular/build/AngularNoNgAttributesSnapshotSerializer.js',
    'jest-preset-angular/build/AngularSnapshotSerializer.js',
    'jest-preset-angular/build/HTMLCommentSerializer.js'
  ]
};
